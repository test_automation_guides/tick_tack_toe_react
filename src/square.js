import React from "react";

export default Square;

function Square(props) {
    return (
        <button id={'square-' + props.number} className="square" onClick={props.onClick}>
            {props.value}
        </button>
    );
}
