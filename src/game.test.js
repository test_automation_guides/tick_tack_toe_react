import React from 'react';
import ReactDOM from 'react-dom';
import Game from "./game";
import calculateWinner from './calculateWinner';

it('renders without crashing', () => {
    const root = document.createElement('root');
    ReactDOM.render(<Game />, root);
});

it('starts with no winner', () => {
    let squares = Array(9).fill(null);
    const winner = calculateWinner(squares);
    expect(winner).toEqual(null);
});

it('winner is x horizontally', () => {
    const squares = Array(9).fill(null);
    squares[0] = 'X';
    squares[1] = 'X';
    squares[2] = 'X';
    const winner = calculateWinner(squares);
    expect(winner).toEqual('X');
});

it('winner is O vertically', () => {
    const squares = Array(9).fill(null);
    squares[0] = 'O';
    squares[3] = 'O';
    squares[6] = 'O';
    const winner = calculateWinner(squares);
    expect(winner).toEqual('O');
});
