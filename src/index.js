import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { hydrate, render } from "react-dom";
import Game from "./game"

// ========================================

/*
ReactDOM.render(
    <Game />,
    document.getElementById('root')
);
 */



const rootElement = document.getElementById("root");
if (rootElement.hasChildNodes()) {
    hydrate(<Game />, rootElement);
} else {
    render(<Game />, rootElement);
}
