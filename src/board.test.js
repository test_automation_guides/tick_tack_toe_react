import React from 'react';
import ReactDOM from 'react-dom';
import Board from "./board";

it('renders without crashing', () => {
    const root = document.createElement('root');
    ReactDOM.render(<Board squares={ Array(9).fill(null) } />, root);
});
