import React from 'react';
import ReactDOM from 'react-dom';
import Square from './square';

it('renders without crashing', () => {
    const root = document.createElement('root');
    ReactDOM.render(<Square />, root);
});
